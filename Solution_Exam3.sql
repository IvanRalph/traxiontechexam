USE `adventureworks`;
DROP procedure IF EXISTS `total_sales_per_territory`;

DELIMITER $$
USE `adventureworks`$$
CREATE PROCEDURE `total_sales_per_territory`(month INT)
BEGIN
	SET @query = CONCAT('SELECT 
		salesterritory.TerritoryID, 
		MAX(salesorderdetail.UnitPrice * salesorderdetail.OrderQty) AS total_sales
	FROM salesterritory
	INNER JOIN salesorderheader
	ON salesterritory.TerritoryID = salesorderheader.TerritoryID
	INNER JOIN salesorderdetail
	ON salesorderheader.SalesOrderID = salesorderdetail.SalesOrderID
	WHERE MONTH(salesorderdetail.ModifiedDate) = ',  month,'
	GROUP BY salesterritory.TerritoryID
	ORDER BY total_sales DESC;');
    PREPARE stmt FROM @query;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END$$

DELIMITER ;

