SELECT product.ProductID, salesorderdetail.OrderQty, productcategory.ProductCategoryID, salesorderdetail.ModifiedDate
FROM product
INNER JOIN productsubcategory
ON product.ProductSubcategoryID = productsubcategory.ProductSubcategoryID
INNER JOIN productcategory
ON productsubcategory.ProductCategoryID = productcategory.ProductCategoryID
INNER JOIN salesorderdetail
ON salesorderdetail.ProductID = product.ProductID
ORDER BY YEAR(salesorderdetail.ModifiedDate), MONTH(salesorderdetail.ModifiedDate), salesorderdetail.ModifiedDate, productcategory.ProductCategoryID, salesorderdetail.OrderQty DESC;