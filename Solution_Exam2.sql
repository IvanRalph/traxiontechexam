SELECT 
	salesperson.SalesPersonID, 
    salesperson.TerritoryID, 
    MAX(salesorderdetail.UnitPrice * salesorderdetail.OrderQty) AS total_sales
FROM salesperson
INNER JOIN salesorderheader
ON salesperson.SalesPersonID = salesorderheader.SalesPersonID
INNER JOIN salesorderdetail
ON salesorderheader.SalesOrderID = salesorderdetail.SalesOrderID
WHERE salesperson.TerritoryID IS NOT NULL
GROUP BY salesperson.TerritoryID
ORDER BY salesperson.TerritoryID, total_sales DESC;